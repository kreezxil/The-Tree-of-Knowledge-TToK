# The Tree of Knowledge TToK

## Notes from Discord

```
[7:49 AM] Law: How do I get past the serpent god if I can't craft weapons armor or tools?  Do you make him slower so that I can break, grab and run?
[7:49 AM] Kreezxil: ok, page secured
[7:50 AM] Kreezxil: we'll figure that out
[7:50 AM] Law: lol
[7:50 AM] Kreezxil: you probably run up to the tree and harvest the fruit
[7:50 AM] Kreezxil: then run your  naked fanny down the mountain until you're hungry enough to eat it.
[7:50 AM] Kreezxil: now you can craft
[7:50 AM] Kreezxil: and then you can go back and fight him.
[7:50 AM] Law: sounds about right.
[7:51 AM] Law: and them fighting gives what reward ?
[7:51 AM] Law: I've already got the fruit ... ? Maybe make the logs unbreakable until the serpent is defeated?
[7:51 AM] Kreezxil: a sense of well being knowing that your offspring will be born in sin.
[7:52 AM] Law: LOL.
[7:52 AM] Kreezxil: hmm, default pretrified wood
@Kreezxil
and fabric versions start almost a week or sooner after each snapshot
[7:52 AM] ZeroNoRyouki: Yeah but you still need to wait for mods to port
[7:52 AM] ZeroNoRyouki: Not sure what the net gain is
[7:52 AM] Kreezxil: mabe you get the Staff of Opposition
[7:52 AM] Kreezxil: whatever you hit with it that is living becomes opposite
[7:52 AM] Kreezxil: and it has an AOE
[7:53 AM] Kreezxil: so if you hit passive pigs with it, they all become hostlile zombie pigmen
[7:53 AM] Kreezxil: but it has limited charges
[7:54 AM] Kreezxil: can't have folks  running arond the nether turning pigmen into common pigs
[7:54 AM] Law: Maybe a "Gilded Tongue" totem which allows you to charm (tame/friend) any entity?
[7:55 AM] Law: limited use of course.
[7:55 AM] Law: But it would explain why serpent tried to coax you into eating the fruit.
[7:56 AM] Kreezxil: that sounds better
[7:56 AM] Law: Maybe serpent has the ability to turn nearby passive creatures hostile to you. Making it a bit more of a fight, when you are ready.
[7:58 AM] Kreezxil: what if it it's hostile, it erects a hedge around you, it and the tree.
[7:58 AM] Kreezxil: a 3 block tall hedge
[7:59 AM] Kreezxil: but then we're back to no tools to kill it
[7:59 AM] Kreezxil: oh
[7:59 AM] Kreezxil: you kill it with love
[7:59 AM] Law: yep.   You can still run away if he's turning cows and pigs hostile. They also have a follow range.
[7:59 AM] Law: ... ???
[8:00 AM] Kreezxil: hmm
[8:00 AM] Kreezxil: ok no hedge
[8:00 AM] Kreezxil: just the forked/guilded tongue and the angry passives
[8:00 AM] Law: Then you get a decent item very early game. I like the idea of coming back to kill it.
[8:00 AM] Law: It kinda gives its own motivation to get through early game.  "Dang snake tried to kill me, I'm going to show it ... "  type thing.
[8:00 AM] Kreezxil: Thinkwood could be the equivalent of a coal block too in burn power.
[8:01 AM] Kreezxil: givign a reason to even cultivate the tree on some type of scale.
[8:01 AM] Law: maybe, but possibly a replacement for lapis in enchangement table?
[8:01 AM] Kreezxil: or that's even better
[8:01 AM] Law: I've never understood lapis.  Or the fruit as a replacement for lapis.
[8:02 AM] Law: A little more scarce than the wood.
[8:02 AM] Kreezxil: you should tag yourself to "Law [Ideaman]" (edited)
[8:02 AM] Law: LOL.
[8:02 AM] Kreezxil: i could have all these "or" concepts and they are all configurable
[8:02 AM] Kreezxil: a certain set will be on by default
[8:03 AM] Law: sounds like a great mod for early progression for a modpack. Build up, kill snake man, once you get the charm, stuff unlocks to progress forward.
[8:04 AM] Kreezxil: so literally first achievement is a quest
[8:04 AM] Law: yep
[8:04 AM] Kreezxil: you can still build a log house to survive the night
[8:05 AM] Law: yep, and break leaves for some apples to eat.
[8:06 AM] Kreezxil:
 # in the beginning
 # if false the fruit must be eaten first
 can_build_a_bed = false
[8:06 AM] Kreezxil: people will be hunting villages for the beds, and early equipment
[8:08 AM] Law: yep. If you really wanted to be cruel, you could cancel the use of tools until they eat the fruit.
[8:08 AM] Kreezxil: i thought canceling all the receips was the ultimate in cruelty
[8:09 AM] Kreezxil: sur eyou can find a tool, but not stop you from actually using htem
[8:12 AM] Law: yea, I can see that. Maybe there could be a slight hint at the grab-n-dash so players don't assume you HAVE to kill the serpent to unlock the recipes.
[8:12 AM] CryoTech: That would be the amnesia froot
[8:12 AM] Law: LOL.
[8:12 AM] Law: Oh man, two types of fruit. One of knowledge and one of forgetfulness. GRab and Dash with the wrong one ... uh oh!
[8:13 AM] CryoTech: Amnesia fruit
Forget everything

Knowledge fruit
Relearn all lost knowledge and how to use tools and benchs
[8:13 AM] Law: And they look the same so its a crapshoot on which one you have.
[8:14 AM] Kreezxil: right intercept the right click event and have it cancel unless the fruit was eaten.
[8:15 AM] CryoTech: Amnesia fruit should be kept with default hunger tbh
[8:15 AM] CryoTech: If ur hungry enough and that's all you have than you need to eat
[8:16 AM] Law: intercept block_break event unless the fruit was eaten, except wood.
[8:17 AM] Kreezxil: maybe it givers 4 sat, but not more than that, like if you eat more, the chat bar says
You try to eat more amnesia fruit, but your body forgot how to digest, so ...
8:17 AM] CryoTech: Effect: nausia/poison
Duration:30sec sickness/hunger drain
```
